# NODE-QRIS

QRIS (Quick Response Code Indonesian Standard) parser library for Node.JS

## Installation

Use the package manager [npm](https://www.npmjs.com/) to install node-qris.

```bash
npm install node-qris
```

## Usage

Use the package manager [npm](https://www.npmjs.com/) to install node-qris.

```nodejs
const parse = require('node-qris');

let qrisData = '00020101021226640013COM.MYWEB.WWW01181234567890123456780214123456789012340303UKE5405100005912QRIS WANTUNO6013Jakarta Pusat6304XXXX';

console.log(parse(qrisData));

```