function parse(data) {

    var tags = _parseTag(data);

    return tags;

}

function _parseTag(emvdata) {

    var tags = [];
    var index = 0;
    var data = emvdata;
    while (index < emvdata.length) {

        var data = emvdata.substring(index);
        var tagId = data.substr(0, 2);
        var tagLength = parseInt(data.substr(2, 2));
        var tagInformation = data.substr(4, tagLength);

        if(tagId === '26'){

            let qris26 = qris26Parse(tagInformation);
            
            qris26.map((q) => {

                tags.push(q);

            });

        }

        if(tagId === '51'){

           

        }
        
        if(typeof emvTags[tagId] !== 'undefined'){

            var tag = {
                "name": emvTags[tagId].name,
                "length": tagLength,
                "data": tagInformation
            }
            
            tags.push(tag);

        }

        index += tagLength + 4;

    }
    return tags;
}

function qris26Parse(qrisdata){

    let tags = [];
    let index = 0;
    
    while (index < qrisdata.length) {

        var data = qrisdata.substring(index);
        var tagId = data.substr(0, 2);
        var tagLength = parseInt(data.substr(2, 2));
        var tagInformation = data.substr(4, tagLength);

        if(tagId === '00'){

            var tag = {
                "name": "QRISIssuer",
                "length": tagLength,
                "data": tagInformation
            }
            
            tags.push(tag);

        }

        if(tagId === '01'){

            var tag = {
                "name": "QRISGlobalMerchantID",
                "length": tagLength,
                "data": tagInformation
            }
            
            tags.push(tag);

        }

        if(tagId === '02'){

            var tag = {
                "name": "QRISAcquirerMerchantID",
                "length": tagLength,
                "data": tagInformation
            }
            
            tags.push(tag);

        }

        if(tagId === '03'){

            var tag = {
                "name": "MerchantBusinessType",
                "length": tagLength,
                "data": tagInformation
            }
            
            tags.push(tag);

        }

        index += tagLength + 4;

    }

    return tags;

}

const emvTags = {
    "00": {
        "id": "00",
        "name": "PayloadFormatIndicator",
        "format": "N",
        "presence": "M",
    }, 
    "01": {
        "id": "01",
        "name": "PointOfInitiationMethod",
        "format": "N",
        "presence": "O"
    },
    "52": {
        "id": "52",
        "name": "MerchantCategoryCode",
        "format": "N",
        "presence": "M"
    }, 
    "53": {
        "id": "53",
        "name": "TransactionCurrency",
        "format": "N",
        "presence": "M"
    }, 
    "54": {
        "id": "54",
        "name": "TransactionAmount",
        "format": "ans",
        "presence": "C"
    },
    "58": {
        "id": "58",
        "name": "CountryCode",
        "format": "ans",
        "presence": "M"
    },
    "59": {
        "id": "59",
        "name": "MerchantName",
        "format": "ans",
        "presence": "M"
    },
    "60": {
        "id": "60",
        "name": "MerchantCity",
        "format": "ans",
        "presence": "M"
    },
    "61": {
        "id": "61",
        "name": "PostalCode",
        "format": "ans",
        "presence": "O"
    },
    "63": {
        "id": "63",
        "name": "CRC",
        "format": "ans",
        "presence": "M",
        "valid": true // check that it valid CRC
    }
}

module.exports = parse;